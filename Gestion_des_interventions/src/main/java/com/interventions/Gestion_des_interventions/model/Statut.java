package com.interventions.Gestion_des_interventions.model;

import jakarta.persistence.Entity;
import lombok.Data;

public enum Statut {

    nonTraite,
    enCours,
    traite
}
