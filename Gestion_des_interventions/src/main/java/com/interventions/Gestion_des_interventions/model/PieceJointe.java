package com.interventions.Gestion_des_interventions.model;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Blob;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "piecejointe")
public class PieceJointe {

    @Id
    @Column(name = "idPieceJointe")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idPieceJointe;

    @Column(name = "nomPieceJointe")
    private String nomPieceJointe;

    @Lob
    @Column(name = "pieceJointe")
    private Byte pieceJointe;

    @ManyToOne
    @MapsId("idIntervention")
    Intervention intervention;
}
