package com.interventions.Gestion_des_interventions.model;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Date;


@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
//@IdClass(HistoriqueId.class)
@Table(name = "historique")
public class Historique {
/*
    @Id
    @Column(name = "interventionId")
    private Long interventionId;

    @Id
    @Column(name = "etudiantId")
    private long etudiantId;
*/
    @Enumerated(EnumType.STRING)
    @Column(name = "statut")
    private Statut statut;

    @Id
    @Column(name = "dateModification")
    private Date dateModification;

    @Id
    @ManyToOne
    @MapsId("idIntervention")
    Intervention intervention;

    @Id
    @ManyToOne
    @MapsId("code")
    Etudiant etudiant;


}
