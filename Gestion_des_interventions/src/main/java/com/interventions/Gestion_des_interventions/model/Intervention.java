package com.interventions.Gestion_des_interventions.model;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Date;
import java.util.Set;


@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "Intervention")
public class Intervention {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idIntervention")
    private Long idIntervention;

    @Column(name = "dateCreationIntervention")
    private Date dateCreationInter;

    @Enumerated(EnumType.STRING)
    @Column(name = "statut")
    private Statut statut;

    @Column(name = "description")
    private String description;

    @ManyToOne
    private Departement departement;

    @OneToMany(mappedBy = "intervention")
    Set<Historique> historiques;

    @OneToOne
    Reponse reponse;

    @OneToMany
    Set<PieceJointe> pieceJointes;

    @ManyToOne
    private Personnel personnel;

}
