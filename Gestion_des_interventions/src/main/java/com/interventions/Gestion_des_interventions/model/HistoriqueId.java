package com.interventions.Gestion_des_interventions.model;

import java.io.Serializable;
import java.sql.Date;

public class HistoriqueId implements Serializable {

    private long interventionId;
    private long etudiantId;
    private Date dateModification;
}