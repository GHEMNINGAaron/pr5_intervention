package com.interventions.Gestion_des_interventions.model;


import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import java.util.Set;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name="role")
public class Departement {

    @Id
    @Column(name = "code")
    private Long codeDepartement;

    @Column(name = "description")
    private String description;

    @OneToMany
    Set<Intervention> intervention;

    @ManyToMany
    @JoinTable(name = "utilisateur_role",
            joinColumns = @JoinColumn(name = "code_utilisateur"),
            inverseJoinColumns = @JoinColumn(name = "code_role"))
    Set<Personnel> personnel;
}
