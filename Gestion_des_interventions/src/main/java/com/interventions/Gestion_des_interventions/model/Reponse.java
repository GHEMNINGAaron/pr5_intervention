package com.interventions.Gestion_des_interventions.model;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


import java.sql.Date;
import java.util.Set;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "reponse")
public class Reponse {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idReponse")
    private int idReponse;

    @Column(name = "dateCreationRep")
    private Date dateCreationRep;

    @Column(name = "description")
    private String description;

    @OneToOne
    Intervention intervention;
}
