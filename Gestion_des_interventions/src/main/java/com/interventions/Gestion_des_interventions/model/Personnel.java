package com.interventions.Gestion_des_interventions.model;


import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.Set;


@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "utilisateur")
public class Personnel {

    @Id
    @Column(name = "code")
    private long idPersonnel;

    @Column(name = "login")
    private String loginPersonel;

    @Column(name = "mot_de_passe")
    private String mdpPersonnel;

    @ManyToMany(mappedBy = "personnel")
    Set<Departement> departements;

    @OneToMany
    Set<Intervention> interventions;
}
