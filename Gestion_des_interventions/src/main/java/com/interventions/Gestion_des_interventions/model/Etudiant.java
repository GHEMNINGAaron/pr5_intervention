package com.interventions.Gestion_des_interventions.model;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.Set;


@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name="etudiant")
public class Etudiant  {

    @Id
    @Column(name="code")
    private long code;

    @Column(name="matricule")
    private String matricule;

    @Column(name = "code_authentification")
    private String codeAuthentification;

    @OneToMany(mappedBy = "etudiant")
    Set<Historique> historiques;


}
