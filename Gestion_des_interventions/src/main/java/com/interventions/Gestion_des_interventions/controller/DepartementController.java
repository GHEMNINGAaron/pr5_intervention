package com.interventions.Gestion_des_interventions.controller;

import com.interventions.Gestion_des_interventions.model.Departement;
import com.interventions.Gestion_des_interventions.model.Etudiant;
import com.interventions.Gestion_des_interventions.model.Intervention;
import com.interventions.Gestion_des_interventions.repositories.DepartementRepository;
import com.interventions.Gestion_des_interventions.service.DepartementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/departement")
public class DepartementController {

    @Autowired
    DepartementRepository departementRepository;

    @GetMapping("/listeDepartement")
    public Iterable<Departement> getDepartement() {
        return departementRepository.findAll();
    }
}
