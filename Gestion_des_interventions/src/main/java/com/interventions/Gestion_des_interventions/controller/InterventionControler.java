package com.interventions.Gestion_des_interventions.controller;

import com.interventions.Gestion_des_interventions.model.Departement;
import com.interventions.Gestion_des_interventions.model.Etudiant;
import com.interventions.Gestion_des_interventions.model.Intervention;
import com.interventions.Gestion_des_interventions.repositories.InterventionRepository;
import com.interventions.Gestion_des_interventions.service.InterventionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
//@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/api/interventions")
public class InterventionControler {

    @Autowired
    private InterventionService interventionService;

    @GetMapping("/etudiant/{idEtudiant}")
    public ResponseEntity<Iterable<Intervention>> getInterventionsByEtudiant(@PathVariable Etudiant idEtudiant) {
        Iterable<Intervention> interventions = interventionService.getInterventionsEtudiant(idEtudiant);
        return ResponseEntity.ok(interventions);
    }

    @GetMapping("/departement/{codeDepartement}")
    public ResponseEntity<Iterable<Intervention>> getInterventionsByDepartement(@PathVariable Departement codeDepartement) {
        Iterable<Intervention> interventions = interventionService.getInterventionsDepartement(codeDepartement);
        return ResponseEntity.ok(interventions);
    }

    @PostMapping("/Intervention")
    public ResponseEntity<Intervention> addIntervention(@RequestBody Intervention intervention) {
        Intervention newIntervention = interventionService.saveIntervention(intervention);
        return ResponseEntity.status(HttpStatus.CREATED).body(newIntervention);
    }

    @PutMapping("/Intervention/{id}")
    public ResponseEntity<Intervention> updateInterventionStatus(@PathVariable Long id, @RequestBody Intervention intervention) {
        Intervention existingIntervention = interventionService.getIntervention(id);
        if (existingIntervention == null) {
            return ResponseEntity.notFound().build();
        }
        existingIntervention.setStatut(intervention.getStatut());
        Intervention updatedIntervention = interventionService.saveIntervention(existingIntervention);
        return ResponseEntity.ok(updatedIntervention);
    }
}