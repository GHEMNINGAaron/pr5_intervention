package com.interventions.Gestion_des_interventions.repositories;

import com.interventions.Gestion_des_interventions.model.Departement;
import com.interventions.Gestion_des_interventions.model.Etudiant;
import com.interventions.Gestion_des_interventions.model.Intervention;
import com.interventions.Gestion_des_interventions.model.Personnel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface InterventionRepository extends JpaRepository<Intervention, Long> {

    Iterable<Intervention> findInterventionByDepartement(Departement departement);

    Iterable<Intervention> findInterventionByHistoriques_Etudiant(Etudiant idEtudiant);


}
