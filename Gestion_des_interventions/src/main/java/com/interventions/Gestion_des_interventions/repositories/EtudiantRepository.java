package com.interventions.Gestion_des_interventions.repositories;

import com.interventions.Gestion_des_interventions.model.Etudiant;
import jakarta.persistence.criteria.CriteriaBuilder;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EtudiantRepository extends JpaRepository<Etudiant, Long> {

    Etudiant findByMatricule(String matricule);
}
