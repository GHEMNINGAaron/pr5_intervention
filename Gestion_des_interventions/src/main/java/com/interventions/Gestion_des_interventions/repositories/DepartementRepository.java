package com.interventions.Gestion_des_interventions.repositories;

import com.interventions.Gestion_des_interventions.model.Departement;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DepartementRepository extends JpaRepository<Departement, Long> {
}
