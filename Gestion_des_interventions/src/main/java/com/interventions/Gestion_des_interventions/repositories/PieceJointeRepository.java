package com.interventions.Gestion_des_interventions.repositories;

import com.interventions.Gestion_des_interventions.model.PieceJointe;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PieceJointeRepository extends JpaRepository<PieceJointe, Integer> {
}
