package com.interventions.Gestion_des_interventions.repositories;

import com.interventions.Gestion_des_interventions.model.Personnel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PersonnelRepository extends JpaRepository<Personnel, Long> {

    Personnel findByLoginPersonel(String loginPersonel);
}
