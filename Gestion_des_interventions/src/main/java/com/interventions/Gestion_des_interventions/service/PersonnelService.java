package com.interventions.Gestion_des_interventions.service;

import com.interventions.Gestion_des_interventions.model.Personnel;
import com.interventions.Gestion_des_interventions.repositories.PersonnelRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PersonnelService {

    @Autowired
    private PersonnelRepository personnelRepository;

    public Iterable<Personnel> getPersonnels(){
        return personnelRepository.findAll();
    }

    public Personnel getpersonnel(final long id){
        return personnelRepository.getReferenceById(id);
    }
}
