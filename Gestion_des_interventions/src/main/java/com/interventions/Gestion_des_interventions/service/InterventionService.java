package com.interventions.Gestion_des_interventions.service;

import com.interventions.Gestion_des_interventions.model.Departement;
import com.interventions.Gestion_des_interventions.model.Etudiant;
import com.interventions.Gestion_des_interventions.model.Intervention;
import com.interventions.Gestion_des_interventions.repositories.InterventionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class InterventionService {

    @Autowired
    private InterventionRepository interventionRepository;


    public Iterable<Intervention> getInterventions(){
        return interventionRepository.findAll();
    }

    public Intervention getIntervention(final Long id){
        return interventionRepository.getReferenceById(id);
    }

    public Iterable<Intervention> getInterventionsEtudiant(final Etudiant idEtudiant){
        return interventionRepository.findInterventionByHistoriques_Etudiant(idEtudiant);
    }

    public Iterable<Intervention> getInterventionsDepartement(Departement departement){
        return interventionRepository.findInterventionByDepartement(departement);
    }

    public Intervention saveIntervention(Intervention intervention){
        return interventionRepository.save(intervention);
    }
}
